#AVG 3d7@mail.ru
#
#Tic-Tac-Toe ( coffeeScript demo game )
#
$ ->
  TicTacToe =
    data:
      moveCount: 0
      x: {}
      o: {}
      gameOver: false
      canComputerMove: false
      boardPattern:
       diagonals:   [[0,4,8], [2,4,6]]
       horizontals: [[0,1,2], [3,4,5], [6,7,8]]
       verticals:   [[0,3,6], [1,4,7], [2,5,8]]

    initialize: ->
      @data.gameOver = false
      @.setPlayerNames()
      @.assignRoles()
      @.initBoard()
      @.addListeners()

    setPlayerNames: ->
      @data.player1 = "Пользователь"
      @data.player2 = "Компьютер"

    getPlayerName: (symbol) ->
      name = if @data.playerRole_1 == symbol then @data.player1 else @data.player2
      return name

    initBoard: ->
      $(".game-board").empty()
      $(".message").text("#{@.getPlayerName("X")} начинает первым!")
      $("<div>", {class: "cell"}).appendTo(".game-board") for square in [0..8]

    assignRoles: ->
      @data.playerRole_1 = "X"
      @data.playerRole_2 = "O"

    addListeners: ->
      self = @
      $(".cell").click ->
        if self.data.gameOver is no and not $(@).text().length
          if self.data.moveCount % 2 is 0
            $(@).html("X").addClass("x moved")
          else if self.data.moveCount % 2 isnt 0 then $(@).html("O").addClass("o moved")
          self.data.moveCount++
          self.checkEnd()
          if self.data.gameOver isnt yes and $(".moved").length >= 9 then self.addToScore("none")
          else if self.data.moveCount % 2 isnt 0

            self.data.canComputerMove = true;
            self.autoMove()

    checkEnd : ->
      @.data.x = {}
      @.data.o = {}

      #diagonal
      for diagonal in @.data.boardPattern.diagonals
        for col in diagonal
          @.checkCell(col, 'diagonal')
        @.checkWin()
        @.flushStorage('diagonal')

      for row in [0..2]
        start = row * 3
        middle = (row * 3) + 1
        end = (row * 3) + 2

        #vertical
        @.checkCell(start, 'start')
        @.checkCell(middle, 'middle')
        @.checkCell(end, 'end')
        @.checkWin()

        #horizontal
        for column in [start..end]
          @.checkCell(column, 'horizontal')
        @.checkWin()
        @.flushStorage('horizontal')

    checkMove : ->

      @.data.x = {}
      @.data.o = {}

      for diagonal in @.data.boardPattern.diagonals
        if !@.data.canComputerMove
           break
        for col in diagonal
          @.checkCell(col, 'diagonal')

        if @.checkBeforeWin()
          for col in diagonal
           if @.makeProtectiveMove(col)
             break
        @.data.x = {}
        @.data.o = {}

      for horizontal in @.data.boardPattern.horizontals
        if !@.data.canComputerMove
          break
        for col in horizontal
          @.checkCell(col, 'horizontal')

        if @.checkBeforeWin()
          for col in horizontal
            if @.makeProtectiveMove(col)
              break

        @.data.x = {}
        @.data.o = {}

      for vertical in @.data.boardPattern.verticals
        if !@.data.canComputerMove
          break
        for col in vertical
          @.checkCell(col, 'vertical')

        if @.checkBeforeWin()
          for col in vertical
            if @.makeProtectiveMove(col)
              break
        @.data.x = {}
        @.data.o = {}

      if @.data.canComputerMove
        selection = $(".cell:not(.moved)")
        @.data.canComputerMove = false;
        selection.eq(Math.floor( Math.random() * selection.length )).click()

    makeProtectiveMove: (cell) ->
      if !@.data.canComputerMove
        return false;
      current = $(".cell").eq(cell)
      if !current.hasClass("moved")
        @.data.canComputerMove = false
        current.click()
        return true
      return false

    checkCell: (cell, propName) ->
      if $(".cell").eq(cell).hasClass("x")
        if @.data.x[propName]? then @.data.x[propName]++ else @.data.x[propName] = 1
      else if $(".cell").eq(cell).hasClass("o")
        if @.data.o[propName]? then @.data.o[propName]++ else @.data.o[propName] = 1

    checkWin: ->
      for key,value of @.data.x
        if value >= 3
          localStorage.x++
          @.showMessage "#{@.getPlayerName("X")} победил!"
          @.data.gameOver = true
          @.addToScore("X")
      for key,value of @.data.o
        if value >= 3
          localStorage.o++
          @.showMessage "#{@.getPlayerName("O")} победил!"
          @.data.gameOver = true
          @.addToScore("O")

    checkBeforeWin: ->
      if @.data.gameOver
        return false
      for key,value of @.data.o
        if value is 2
          return true
      for key,value of @.data.x
        if value is 2
          return true
      return false

    addToScore: (winner) ->
      @data.moveCount = 0
      @data.x = {}
      @data.o = {}
      @data.gameOver = yes
      if winner is "none"
        @.showMessage "Ничья!"
      else
      $(".play-again").show()
      $(".cell").off()

    flushStorage: (propName) ->
      @.data.x[propName] = null
      @.data.o[propName] = null

    showMessage: (msg) ->
      $(".message").text(msg)

    autoMove: () ->
      @.checkMove()


  $(document).ready ->
    TicTacToe.initialize()
    $(".play-again").click ->
      TicTacToe.initialize()
      $(".play-again").hide()
      event.stopPropagation()
